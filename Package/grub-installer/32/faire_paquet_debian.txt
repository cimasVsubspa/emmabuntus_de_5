# Outil à installer :
# sudo apt-get install debhelper cdbs lintian build-essential fakeroot devscripts pbuilder dh-make debootstrap
#
# https://www.debian-fr.org/t/extraire-paquet-deb/31870
#
# Pour extraire le paquet :
#
# dpkg-deb -x paquet.deb repertoire -> extrait l'arborescence
# dpkg-deb -e paquet.deb -> extrait le répertoire DEBIAN contenant les différents fichiers postinst, control, etc
#
# Pour assembler le paquet :
#
# dpkg-deb -b repertoire paquet.deb
#

# Méthode paquet origine

version=1.188_i386
nom_paquet=grub-installer
ext_paquet=udeb

mkdir ${nom_paquet}
dpkg-deb -x ${nom_paquet}_${version}.${ext_paquet} ${nom_paquet}
dpkg-deb -e ${nom_paquet}_${version}.${ext_paquet} ${nom_paquet}/DEBIAN


# Ajout de ces options pour installer le grub sans acquitement de changement et en mode UEFI, voir ligne 528 :

    # Will pull in os-prober based on global setting for Recommends
    in-target apt install -y -o apt::install-recommends=true $grub_package || exit_code=$?

    à la place de :

    # Will pull in os-prober based on global setting for Recommends
    apt-install $grub_package || exit_code=$?


# Méthode assemblage

dpkg-deb -b ${nom_paquet} ${nom_paquet}_${version}_mod_emma.${ext_paquet}
cp ${nom_paquet}_${version}_mod_emma.${ext_paquet} ../../../arch/32/packages.installer_debian-installer.udeb/${nom_paquet}_${version}.${ext_paquet}
