#! /bin/bash

# install_calibri_fonts_wrapper.sh --
#
#   This file permits to install nofree Calibri Microsoft Fonts for the Emmabuntüs Equitable Distribs.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

clear

nom_distribution="Emmabuntus Debian Edition 5"

nom_logiciel_affichage="Calibri Microsoft"
nom_paquet=install_calibri_fonts_display.sh

dir_install_non_free_softwares=/opt/Install_non_free_softwares
dir_install_fonts=/usr/share/fonts/truetype/fonts_MS_Office_2007
delai_fenetre=20



#  chargement des messages
. /opt/Install_non_free_softwares/emmabuntus_messages.sh


if test -z "$message_charge" ; then

    echo "# $nom_logiciel_affichage software installation canceled : Load message KO"

    exit 0

fi


if test -f ${dir_install_fonts}/Calibri.ttf
then

    echo "# $nom_logiciel_affichage is already installed"

    export WINDOW_DIALOG_SOFTWARE_INSTALLED='<window title="" icon-name="gtk-info" resizable="false">
    <vbox spacing="0">

    <text use-markup="true" wrap="false" xalign="0" justify="3">
    <input>echo "\n'$nom_logiciel_affichage' '$msg_installed'     \n" | sed "s%\\\%%g"</input>
    </text>

    <hbox spacing="10" space-expand="false" space-fill="false">
    <button can-default="true" has-default="true" use-stock="true" is-focus="true">
    <label>gtk-ok</label>
    <action>exit:OK</action>
    </button>
    </hbox>

    </vbox>
    </window>'

    MENU_DIALOG_DIALOG_SOFTWARE_INSTALLED="$(gtkdialog --center --program=WINDOW_DIALOG_SOFTWARE_INSTALLED)"


    exit 0

fi



export WINDOW_DIALOG_INSTALL_NO_FREE_SOFTWARE='<window title="'${nom_logiciel_affichage}'" icon-name="gtk-dialog-question" resizable="false">
<vbox spacing="0">

<text use-markup="true" wrap="false" xalign="0" justify="3">
<input>echo "'$message_demarrage_soft_non_libre'" | sed "s%\\\%%g"</input>
</text>

<hbox spacing="10" space-expand="false" space-fill="false">
<button cancel></button>
<button can-default="true" has-default="true" use-stock="true" is-focus="true">
<label>gtk-ok</label>
<action>exit:OK</action>
</button>
</hbox>

</vbox>
</window>'

MENU_DIALOG_DIALOG_INSTALL_NO_FREE_SOFTWARE="$(gtkdialog --center --program=WINDOW_DIALOG_INSTALL_NO_FREE_SOFTWARE)"

eval ${MENU_DIALOG_DIALOG_INSTALL_NO_FREE_SOFTWARE}


if [[ ${EXIT} != "OK" ]]
then

    echo "# $nom_logiciel_affichage software installation canceled"

    exit 0

else

    pkexec $dir_install_non_free_softwares/$nom_paquet

fi


