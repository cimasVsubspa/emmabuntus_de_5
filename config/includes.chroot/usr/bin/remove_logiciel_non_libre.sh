#! /bin/bash

# remove_logiciel_non_libre.sh --
#
#   This file permits to remove non-free softwares for the Emmabuntus Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

export SUDO_ASKPASS=/usr/bin/gksudo

[ ! "${SUDO_USER}" ] && { cd "$(dirname "$0")"; exec gksu -S -- "./${0##*/}"; }


sudo apt-get autoremove --purge -y unrar
sudo apt-get autoremove --purge -y w32codecs
sudo apt-get autoremove --purge -y w64codecs
sudo apt-get autoremove --purge -y libdvdcss2
sudo apt-get autoremove --purge -y libdvdread4





