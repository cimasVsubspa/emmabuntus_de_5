#! /bin/bash


# change_language.sh --
#
#   This file permits to change language used for the Emmabuntüs Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


########################################################################################################

. "$HOME/.config/user-dirs.dirs"


clear

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


nom_distribution="Emmabuntus Debian Edition 5"
nom_titre_window="Change languages"
repertoire_emmabuntus=~/.config/emmabuntus
fichier_init_config_user=${repertoire_emmabuntus}/init_config_remove_language_not_use.txt
dir_install_non_free_softwares=/opt/Install_non_free_softwares
fichier_init_config_final=$dir_install_non_free_softwares/.init_config_remove_language_not_use.txt
repertoire_config_language=/usr/share/emmabuntus/

CHECKBOX_RESTART="true"
CHECKBOX_INSTALL="true"
CHECKBOX_SET_DEFAULT_LANG="true"
CHECKBOX_INTERNET_ACTIF="false"

choix=""
utilisateur=emmabuntus

source /usr/bin/emmabuntus_found_theme.sh
highlight_color=$(FOUND_HIGHLIGHT_COLOR)


message_demarrage="\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Do you want to change language in the distribution ?')\</span>"

message_validation="\
$(eval_gettext 'Please select the language you need:')"


if [[ $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then
    echo "Internet is a live"
    CHECKBOX_INTERNET_ACTIF="true"
else
    echo "Internet wasn't a live !!"
    CHECKBOX_INTERNET_ACTIF="false"
    CHECKBOX_INSTALL="false"
fi


# Determination du pays courant
locale=$(echo $LANG | cut -d '.' -f1)

if [[ ${locale} == "" ]] ; then
    locale=${LANG}
fi

echo "LANG=${LANG}"

COUNTRY=$(grep ^${locale} ${repertoire_config_language}/language_locale | cut -d '"' -f2)

if [[ ${COUNTRY} == "" ]] ; then
    locale="en_US"
    COUNTRY=$(grep ^${locale} ${repertoire_config_language}/language_locale | cut -d '"' -f2)
fi

echo "COUNTRY=${COUNTRY}"


 export WINDOW_DIALOG='<window title="'$(eval_gettext 'Language Management')'" icon-name="gtk-dialog-question" width_request="650"  height_request="220" resizable="false">
 <vbox spacing="0">
 <text use-markup="true" wrap="false" xalign="0" justify="3">
 <input>echo "'${message_demarrage}'" | sed "s%\\\%%g"</input>
 </text>
 <text use-markup="true" wrap="false" xalign="0" justify="3">
 <input>echo "'${message_validation}'" | sed "s%\\\%%g"</input>
 </text>

 <hseparator space-expand="true" space-fill="true"></hseparator>

   <checkbox active="'${CHECKBOX_INSTALL}'" sensitive="'${CHECKBOX_INTERNET_ACTIF}'" tooltip-text="'$(eval_gettext 'Install additional language packs for the selected language in the drop-down menu below. Requires an active Internet connection.')'">
   <variable>CHECKBOX_INSTALL</variable>
   <label>"'$(eval_gettext 'Install the complements of the selected language below:')'"</label>
   </checkbox>

   <comboboxtext allow-empty="false" value-in-list="true">
   <variable>SELECT_COUNTRY</variable>
   <default>'${COUNTRY}'</default>
   <input file>'${repertoire_config_language}/countries'</input>
   </comboboxtext>

   <checkbox active="'${CHECKBOX_SET_DEFAULT_LANG}'" tooltip-text="'$(eval_gettext 'Requires a computer restart to take the new language into account')'">
   <variable>CHECKBOX_SET_DEFAULT_LANG</variable>
   <label>"'$(eval_gettext 'Set the language selected above as the default system language')'"</label>
   </checkbox>

 <hseparator space-expand="true" space-fill="true"></hseparator>

   <hbox spacing="10" space-expand="false" space-fill="false">
   <button cancel></button>
   <button can-default="true" has-default="true" use-stock="true" is-focus="true">
   <label>gtk-ok</label>
   <action>exit:OK</action>
   </button>
   </hbox>


 </vbox>
 </window>'


 MENU_DIALOG="$(gtkdialog --center --program=WINDOW_DIALOG)"

 eval ${MENU_DIALOG}
 echo "MENU_DIALOG=${MENU_DIALOG}"

 if [ ${CHECKBOX_INSTALL} == "true" ]
 then
     choix=${choix}:Install
 fi


 if [ ${CHECKBOX_SET_DEFAULT_LANG} == "true" ]
 then
     choix=${choix}:SetDefaultLanguage
 fi

 echo "choix=${choix}"

 if [ ${EXIT} == "OK" ]
 then
     if [[ ${choix} != "" ]]
     then

         user=$USER

         # Détermination locale sélectionné
         select_locale=$(grep "${SELECT_COUNTRY}" ${repertoire_config_language}/language_locale | cut -d ':' -f1)

         if [[ ${select_locale} == "" ]]; then
         select_locale=${locale}
         fi

         pkexec /usr/bin/remove_language_not_use_exec.sh "${choix}" "${user}" "${select_locale}" "${highlight_color}"

     fi
 fi

