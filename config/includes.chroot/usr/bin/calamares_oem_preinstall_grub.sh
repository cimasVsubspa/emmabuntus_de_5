#! /bin/bash

# calamares_oem_preinstall_grub.sh --
#
#   This file permits to prepare OEM install for the Emmabuntus Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

file_config="/etc/calamares/settings.conf"
mount_point="/media/system_patch_hostname"

repertoire_emmabuntus=~/.config/emmabuntus
fichier_init=${repertoire_emmabuntus}/postinstall_calamares.txt
wallpaper_file="/usr/share/xfce4/backdrops/emmabuntus_fond_ecran_ligthdm.jpg"

if [[ $(cat /proc/cmdline | grep -i oem) ]] ; then

    # Mise en place du mot de passe root en live
    echo -e "live\nlive" | sudo passwd

    function FCT_WAIT_PROCESS()
    {
    sleep 2
    while ps axg | grep -vw grep | grep -w $1 > /dev/null; do sleep 1; done
    }

    if [[ -f ${file_config}.preoem ]] ; then

        if [[ -f "${bureau}/install-debian.desktop" ]] ; then
            rm "${bureau}/install-debian.desktop"
        fi

        if ps -A | grep "xfce4-session" ; then

            # Suppression des raccourcis
            xfconf-query -c xfce4-keyboard-shortcuts -p /commands/custom -r -R
            xfconf-query -c xfce4-keyboard-shortcuts -p /commands/default -r -R
            xfconf-query -c xfce4-keyboard-shortcuts -p /xfwm4/custom -r -R
            xfconf-query -c xfce4-keyboard-shortcuts -p /xfwm4/default -r -R

            # Désactivation des actions du desktop
            xfconf-query -c xfce4-desktop -p "/desktop-icons/style" -n -t int -s 0
            xfconf-query -c xfce4-desktop -p "/windowlist-menu/show" -n -t bool -s false
            xfconf-query -c xfce4-desktop -p "/desktop-menu/show" -n -t bool -s false

            # Maj fond d'écran
            xfconf-query -c xfce4-desktop -p "/backdrop/screen0/monitor0/image-path" -s "${wallpaper_file}"
            xfconf-query -c xfce4-desktop -p "/backdrop/screen0/monitor0/last-image" -s "${wallpaper_file}"
            xfconf-query -c xfce4-desktop -p "/backdrop/screen0/monitor0/last-single-image" -s "${wallpaper_file}"
            xfconf-query -c xfce4-desktop -p "/backdrop/screen0/monitor0/image-style" -s "5"

            xfconf-query -c xfce4-desktop -p "/backdrop/screen0/monitor0/workspace0/last-image" -s "${wallpaper_file}"
            xfconf-query -c xfce4-desktop -p "/backdrop/screen0/monitor0/workspace1/last-image" -s "${wallpaper_file}"
            xfconf-query -c xfce4-desktop -p "/backdrop/screen0/monitor0/workspace2/last-image" -s "${wallpaper_file}"
            xfconf-query -c xfce4-desktop -p "/backdrop/screen0/monitor0/workspace3/last-image" -s "${wallpaper_file}"

        fi

        if [[ -f ${file_config} ]] ; then
            sudo mv ${file_config} ${file_config}.bak
        fi

        sudo cp ${file_config}.preoem ${file_config}
        sudo chmod a+r ${file_config}

        sudo install-debian &

        for i in {1..30}
        do

            DATE=`date +"%d:%m:%Y - %H:%M:%S"`
            wmctrl -l >> ${fichier_init}
            echo "${i} - DATE = $DATE" >> ${fichier_init}

            #wmctrl -a Emmabuntüs -b add,fullscreen

            sleep 1

            if [[ -f "${bureau}/install-debian.desktop" ]] ; then
                rm "${bureau}/install-debian.desktop"
            fi

            if ps -A | grep "xfce4-panel" ; then
                pkill xfce4-panel
            fi

            if ps -A | grep "lxqt-panel" ; then
                pkill lxqt-panel
                pkill lxqt-globalkeys
            fi

            #wmctrl -a Emmabuntüs -b add,fullscreen

        done

        FCT_WAIT_PROCESS install-debian

        if [[ -f ${file_config}.bak ]] ; then
            sudo rm ${file_config}
            sudo mv ${file_config}.bak ${file_config}
        fi

        # Patch hostname
        sudo mkdir ${mount_point}
        sudo mount /dev/sda1 ${mount_point}

        if [[ $(cat ${mount_point}/etc/hostname | grep localhost.localdomain) ]] ; then

            echo "oem-pc" | sudo tee ${mount_point}/etc/hostname

            sudo tee ${mount_point}/etc/hosts > /dev/null <<EOT
127.0.0.1   localhost
127.0.0.1   oem-pc.unassigned-domain oem-pc

# The following lines are desirable for IPv6 capable hosts
::1         localhost ip6-localhost ip6-loopback
ff02::1     ip6-allnodes
ff02::2     ip6-allrouters
EOT

        fi

        sudo umount ${mount_point}
        sudo rm -d ${mount_point}

        exit 0

    else

        exit 1

    fi

fi


