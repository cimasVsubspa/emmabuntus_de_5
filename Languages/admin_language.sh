#! /bin/bash
chemin="$(cd "$(dirname "$0")";pwd)/$(basename "$0")";
dossier="$(dirname "$chemin")"
cd "${dossier}"



###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="${dossier}/locale"
. gettext.sh
emmabuntus=$0

#_________________________________________________
#░▒▓███████████████████ADMIN███████████████████▓▒░

#si dans /usr/local/share/emmabuntus
if [[ ! "$SUDO_USER" && "${dossier}" == "/usr/local/share/emmabuntus" ]]; then
#gksudo -k "$chemin" #supprimé plus dispo ...
exit 0
fi

mkdir /tmp/emmabuntus 2>/dev/null


#liste des langues
#VARLANGUES="en fr es de it pt da"
#cat /usr/share/language-selector/data/languagelist
selang="$(cat "${dossier}/lang_list.txt")"
VARLANGUES="$(echo -e "$selang" | awk -F"|" '{print $2}' | xargs)"

##Voir fichier ==> .../lang_list.txt
##see fille ==> .../lang_list.txt

#liste des fichiers a prendre en compte pour traduction des .po
list_po="
../config/includes.chroot/usr/bin/emmabuntus_welcome.sh
../config/includes.chroot/usr/bin/acceptation_install_logiciel_non_libre.sh
../config/includes.chroot/usr/bin/acceptation_install_logiciel_non_libre_exec.sh
../config/includes.chroot/usr/bin/calamares_install.sh
../config/includes.chroot/usr/bin/calamares_oem_switch.sh
../config/includes.chroot/usr/bin/copy_env_user_system_exec.sh
../config/includes.chroot/usr/bin/emmabuntus_choose_keyboard.sh
../config/includes.chroot/usr/bin/emmabuntus_config.sh
../config/includes.chroot/usr/bin/emmabuntus_config_dock.sh
../config/includes.chroot/usr/bin/emmabuntus_config_postinstall.sh
../config/includes.chroot/usr/bin/emmabuntus_create_cache_apt.sh
../config/includes.chroot/usr/bin/emmabuntus_create_cache_apt_exec.sh
../config/includes.chroot/usr/bin/emmabuntus_fix_swap_use.sh
../config/includes.chroot/usr/bin/emmabuntus_fix_swap_use_exec.sh
../config/includes.chroot/usr/bin/emmabuntus_samba.sh
../config/includes.chroot/usr/bin/emmabuntus_tools.sh
../config/includes.chroot/usr/bin/fix_tearing.sh
../config/includes.chroot/usr/bin/init_cairo_dock.sh
../config/includes.chroot/usr/bin/lock_cairo_dock.sh
../config/includes.chroot/usr/bin/enable_cairo_dock.sh
../config/includes.chroot/usr/bin/start_cairo_dock.sh
../config/includes.chroot/usr/bin/remove_language_not_use.sh
../config/includes.chroot/usr/bin/remove_language_not_use_exec.sh
../config/includes.chroot/opt/Turboprint/install_turboprint.sh
../config/includes.chroot/opt/Turboprint/turboprint_wrapper.sh
../config/includes.chroot/opt/gSpeech/autostart_gspeech.sh
../config/includes.chroot/opt/Install_non_free_softwares/emmabuntus_messages.sh
../config/includes.chroot/opt/price_emmaus/prix_emmaus.sh
../config/includes.chroot/opt/price_emmaus/prix_ordi_gui.sh
../config/includes.chroot/opt/ctparental/install_ctparental.sh
../config/includes.chroot/opt/ctparental/install_ctparental_exec.sh
../config/includes.chroot/usr/bin/deborah_wrapper.sh
"
export list_po


#Attention pour que gettext ne se plante pas dans les traductions remplacer ' et \' et '\'' par son equivalent en octal \047
#pour les simple quote: ' ==> \\047
#et pour les double quote: " ==> \\042

#$(eval_gettext "Temps d\047execution: \$MIN Minutes et \$SEC Secondes")
#$(eval_gettext "")
#$(eval_gettext "Choisir l\047option \$var")
#$(eval_gettext '')
#
#syntaxe a adopter dans les fichiers en gtkdialog
#"'$(eval_gettext '')'"
#"'$(eval_gettext '')'"
#"'$(eval_gettext "Editer \$HOME/.md5_live_perso.txt")'"
#"'$(eval_gettext "Chemin de l'iso:")'"
#export LC_ALL=fr_FR.UTF-8
#export LC_ALL="en_US.UTF-8"


#Menage, virer les sauvegardes!
find "${dossier}" | egrep "~$" | perl -n -e 'system("rm $_");'


function internationalisation()
{
#Mettre en place internationalisation
cd "${dossier}"
#Créer les .po
for L in $VARLANGUES
do
if [ ! -e "${dossier}/locale/${L}/LC_MESSAGES/emmabuntus.${L}.po" ]; then
echo -e "Créer: ${L}"
mkdir -p "${dossier}/locale/${L}/LC_MESSAGES"
xgettext --from-code=UTF-8 \
--no-location \
-L shell --no-wrap \
-o "${dossier}/locale/${L}/LC_MESSAGES/emmabuntus.${L}.po" $(printf "$list_po" | xargs)
#Modifier CHARSET
sed -i "s%charset=CHARSET%charset=UTF-8%" "${dossier}"/locale/${L}/LC_MESSAGES/emmabuntus.${L}.po

#Créer les binaires .mo
msgfmt -o "${dossier}"/locale/${L}/LC_MESSAGES/emmabuntus.mo \
"${dossier}"/locale/${L}/LC_MESSAGES/emmabuntus.${L}.po
fi
done
}
internationalisation


#Appliquer manuellement apres chaque modification du script!
function internationalisation_update()
{

#mise à jour. ,regenerer les .po avec en plus les nouveaux champs.
for L in $VARLANGUES
do
xgettext -join-existing \
--no-location \
--from-code=UTF-8 \
--package-name=emmabuntus \
-L shell \
--no-wrap \
-o "${dossier}"/locale/${L}/LC_MESSAGES/emmabuntus.${L}.po $(printf "$list_po" | xargs)
done

#Recréer les .mo
for L in $VARLANGUES
do
msgfmt \
-o "${dossier}"/locale/${L}/LC_MESSAGES/emmabuntus.mo \
"${dossier}"/locale/${L}/LC_MESSAGES/emmabuntus.${L}.po

#Modifier entête
translator=$(echo -e "$selang" | grep \|${L}\| | awk -F"|" '{print $4}')
email=$(echo -e "$selang" | grep \|${L}\| | awk -F"|" '{print $5}')

if [[ ${email} != "" ]] ; then sed -i "s%FULL NAME <EMAIL@ADDRESS>%FULL NAME <${email}>%" "${dossier}"/locale/${L}/LC_MESSAGES/emmabuntus.${L}.po ; fi
if [[ ${translator} != "" ]] ; then sed -i "s%Last-Translator: FULL NAME%Last-Translator: ${translator}%" "${dossier}"/locale/${L}/LC_MESSAGES/emmabuntus.${L}.po ; fi


done

#Retraduire les champs msgstr non traduits avec poedit ou gtranslator
#Editeurs poedit, gtranslator, ...

if [ ! "$(which poedit)" ]; then
sudo apt-get install -y poedit
fi

echo  'zenity \
--title="Live CD" \
--text="Veuillez sélectionner le fichier .po à traduire
dans la liste ci-dessous.
ATTENTION!
Veuillez respecter la longueur des phrases
ainsi que les saut de lignes sous peine
de provoquer des bogs d'\''affichage
ne traduisez pas les variables commençant par $
" \
--width=360 \
--height=400 \
--list \
--print-column="2" \
--radiolist \
--separator=" " \
--column="*" \
--column="Val" \' >/tmp/emmabuntus/emmabuntus-tr
echo -e "$(ls -A "${dossier}"/locale | tr " " "\n" | awk '{print "FALSE " $0 " \\"}')" >>/tmp/emmabuntus/emmabuntus-tr
RETOUR_TRANSLATE=$(. /tmp/emmabuntus/emmabuntus-tr)
if [ -e "${dossier}/locale/${RETOUR_TRANSLATE}/LC_MESSAGES/emmabuntus.${RETOUR_TRANSLATE}.po" ]; then
poedit "${dossier}"/locale/${RETOUR_TRANSLATE}/LC_MESSAGES/emmabuntus.${RETOUR_TRANSLATE}.po
msgfmt -o "${dossier}"/locale/${RETOUR_TRANSLATE}/LC_MESSAGES/emmabuntus.mo \
"${dossier}"/locale/${RETOUR_TRANSLATE}/LC_MESSAGES/emmabuntus.${RETOUR_TRANSLATE}.po
else
#Supprimer
rm -R /tmp/emmabuntus
exit 0
fi

}
internationalisation_update
echo -e "\033[1;47;31m Attention, pensez a faire une copie en local du fichier:${dossier}/locale/*/LC_MESSAGES/emmabuntus.*.po\nCar toute mise à jour n\047integrant pas ce fichier l\047ecrasera\!,\net vous perdrez votre travail... \033[0m"

#Supprimer
rm -R /tmp/emmabuntus
rm ./locale/en/LC_MESSAGES/*.mo

    for L in $VARLANGUES
    do
    file="./locale/${L}/LC_MESSAGES/emmabuntus.${L}.mo"
        if test -f ${file} ; then
        echo "Suppression fichier inutile : ${file}"
        rm ${file}
        fi
    done

exit 0


## #virer commentaires
## find /home/frafa/Documents/sauvegarde/scripts/emmabuntus/emmabuntus/locale/ \
## -iname "*.po" -exec sed -i "s@#: /home/frafa/Documents/sauvegarde/scripts/emmabuntus/emmabuntus/.*@@g" {} \;
## #virer lignes vides
## find /home/frafa/Documents/sauvegarde/scripts/emmabuntus/emmabuntus/locale/ \
## -iname "*.po" -exec sed -i "/^$/d" {} \;
