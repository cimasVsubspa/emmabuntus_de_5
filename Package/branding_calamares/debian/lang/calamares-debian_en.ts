<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>show</name>
    <message>
        <location filename="../show.qml" line="45"/>
        <source>Welcome to Debian 12 GNU/Linux - Emmabuntus DE 5.&lt;br/&gt;The rest of the installation is automated and should complete in a few minutes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../show.qml" line="67"/>
        <source>More customization available through the post-installation process.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../show.qml" line="88"/>
        <source>More accessibility for everyone, thanks to the three dock levels !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../show.qml" line="109"/>
        <source>More than 60 software programs available, which allow the beginners to discover GNU/Linux.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../show.qml" line="130"/>
        <source>More than 10 edutainment software programs for training, including Kiwix the offline Wikipedia reader.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../show.qml" line="151"/>
        <source>Emmabuntus supports social and environmental projects through the Lilo ethical and solidarity research engine.</source>
        <translation type="unfinished"></translation>
    </message>
        <message>
        <location filename="../show.qml" line="171"/>
        <source>Emmabuntus collaborates with more than 10 associations, particularly in Africa, in order to reduce the digital divide.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../show.qml" line="191"/>
        <source>Emmabuntus together with YovoTogo and JUMP Lab'Orione have equipped and run 32 computer rooms in Togo.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../show.qml" line="211"/>
        <source>Emmabuntus is also a refurbishing key, based on Ventoy,&lt;br/&gt;allowing you to quickly and easily refurbish a computer with GNU/Linux.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
