English|en|en_US.UTF-8|Yves Saboret|yves.saboret@gmail.com
French|fr|fr_FR.UTF-8|Emmabuntüs Collective|contact@emmabuntus.org
Spanish|es|es_ES.UTF-8||
Portuguese|pt|pt_PT.UTF-8||
Italian|it|it_IT.UTF-8|Salvo Cirmi|infotuxnewsit@gmail.com
German|de|de_DE.UTF-8|Benoît Boudaud|benoit.boudaud@lilo.org
Danish|da|da_DK.UTF-8|Carl Andersen|carl@bluemarlin.dk
Japan|ja|ja_JP.UTF-8|Kodashima|pc.freedom.plus@gmail.com
